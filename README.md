[![pipeline status](https://gitlab.com/nicosingh/md2gslides/badges/master/pipeline.svg)](https://gitlab.com/nicosingh/md2gslides/commits/master) [![Docker Pulls](https://img.shields.io/docker/pulls/nicosingh/md2gslides.svg)](https://hub.docker.com/r/nicosingh/md2gslides/)

# About

Alpine-based docker image to run [md2googleslides](https://github.com/gsuitedevs/md2googleslides) as a container.

The idea is to avoid installing md2googleslides via npm.

# How to use this Docker image?

To get some help of `md2gslides` command:

`docker run nicosingh/md2gslides -h`

To parse a markdown file, we have to provide it as a volume:

`docker run -it -v <your_full_file_path>:/tmp/slides.md nicosingh/md2gslides -n /tmp/slides.md`

This will prompt us to give Google API credentials before proceeding.

It is recommended to use `--rm` option when running `docker run` command, because after parsing the markdown file the container will not be used anymore.
